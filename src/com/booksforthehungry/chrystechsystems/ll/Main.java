package com.booksforthehungry.chrystechsystems.ll;

public class Main {
    private static Game game = new Game();
    public static void main(String[] args) {
	// write your code here
        game.setup();
        //game.GENERATOR.getRoot();
        try{
            Thread.sleep(1000);
        }catch (InterruptedException IE){
            System.err.println("This shouldn't exist, we are not multithreaded");
        }

        game.playGame();
    }
}
