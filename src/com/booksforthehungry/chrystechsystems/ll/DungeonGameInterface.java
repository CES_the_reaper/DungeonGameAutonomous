package com.booksforthehungry.chrystechsystems.ll;

import java.io.PrintStream;
import java.util.Scanner;

/**
 * Project dungeonGame created on 3/21/2017, Package com.booksforthehungry.chrystechsystems.ll.
 */
public interface DungeonGameInterface {
    int NORTH = 1;
    int EAST = 2;
    int SOUTH = 3;
    int WEST = 4;
    DungeonGenerator GENERATOR = new DungeonGenerator();
    //Dungeon GAMEDUNGEON = new Dungeon();
    Scanner reader = new Scanner(System.in);
    Printer printer = new Printer(System.out);
    public void setup();
    public void playGame();
    public Room getRoot();
    public boolean checkOrb();
    public void move(int Direction);
}
class Printer{
    PrintStream ps = null;
    public Printer(PrintStream ps){
        this.ps = ps;
    }
    public void println(String s){
        ps.println(s);
    }
}