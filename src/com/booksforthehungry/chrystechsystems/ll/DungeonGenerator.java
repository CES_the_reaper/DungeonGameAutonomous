package com.booksforthehungry.chrystechsystems.ll;

/**
 * Project dungeonGame created on 3/21/2017, Package com.booksforthehungry.chrystechsystems.ll.
 */
public class DungeonGenerator implements DungeonGeneratorInterface {
    private Room root = null;
    Room computer = null;
    Room cursorPrevious = null;
    private Room player = null;
    private int depth = 0;
    @Override
    public Room getRoot() {
        return root;
    }
     //   root = null;
    //}
    @Override
    public Room generate(int numberOfRoomsToGenerate, int formulaPush, int counterBalance) {
        //TODO numberOfRooms = iteration of room creation, root is first. Human placed at root, Computer placed at abs(CounterBalance * (1/2*FormulaPush))%numberOfRooms, Treasure placed at abs(FormulaPush-CounterBalance+3*5/4)%numberOfRooms
        root = new Room(1);
        cursorPrevious = root;
        depth = 1;
        player = root;
        Room cursor = root;
        int computerAt = Math.abs(counterBalance * (1/2*formulaPush))%numberOfRoomsToGenerate;
        int treasureAt = Math.abs(formulaPush-counterBalance+3*5/4)%numberOfRoomsToGenerate;
        if(computerAt == 0){
            computer = root;
        }
        for (int i = 2; i <= numberOfRoomsToGenerate; i++){
            Room x = new Room(i);
            if (i == treasureAt){
                x.addTreasure();
            }
            if (i == computerAt){
                if(computerAt == treasureAt){
                    //computerAt = ((computerAt * 7 + Math.abs(counterBalance * (1/2*formulaPush))%numberOfRoomsToGenerate+1)/2)%numberOfRoomsToGenerate+1;
                    if(cursor.north != null){
                        computer = cursor.north;
                    } else if(cursor.south != null){
                        computer = cursor.south;
                    } else if(cursor.east != null){
                        computer = cursor.east;
                    } else if(cursor.west != null){
                        computer = cursor.west;
                    }
                }
                else {
                    computer = x;
                }
            }
            boolean attached = false;
            while(!attached){
                if(!cursor.attachRoom(x)){
                    if (cursor.north != null && cursor.north.getRoomNumber() != cursor.getRoomNumber() && cursor.north.getRoomNumber() != cursorPrevious.getRoomNumber()) {
                        cursorPrevious = cursor;
                        cursor = cursor.north;
                    }
                    else if (cursor.east != null && cursor.east.getRoomNumber() != cursor.getRoomNumber() && cursor.east.getRoomNumber() != cursorPrevious.getRoomNumber()) {
                        cursorPrevious = cursor;
                        cursor = cursor.east;
                    }
                    else if (cursor.south != null && cursor.south.getRoomNumber() != cursor.getRoomNumber() && cursor.south.getRoomNumber() != cursorPrevious.getRoomNumber()) {
                        cursorPrevious = cursor;
                        cursor = cursor.south;
                    }
                    else if (cursor.west != null && cursor.west.getRoomNumber() != cursor.getRoomNumber() && cursor.west.getRoomNumber() != cursorPrevious.getRoomNumber()) {
                        cursorPrevious = cursor;
                        cursor = cursor.west;
                    }
                    else return null;//System.exit(100);
                    System.out.println("Cursor moved to " + cursor.getRoomNumber());
                } else{
                    attached = true;
                }
            }
            if(!x.attachRoom(cursor)){
                System.err.println("Fail Scenario, Restarting Generator: " + x.getRoomNumber());
                return null;
            }
        }
        return root;
    }

    @Override
    public int randomPlacement(int itemType, int formulaPush, int counterBalance) {
        return 0;
    }
}
