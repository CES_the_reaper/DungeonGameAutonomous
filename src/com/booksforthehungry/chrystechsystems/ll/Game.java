package com.booksforthehungry.chrystechsystems.ll;

import java.util.Random;
import java.util.Scanner;

/**
 * Project dungeonGame created on 3/21/2017, Package com.booksforthehungry.chrystechsystems.ll.
 */
public class Game implements DungeonGameInterface {
    Room current = null;
    Room computer = null;
    public static final int FORMULAPUSH = 9832;
    public static final int COUNTERBALANCE = -28;

    @Override
    public void setup() {
        int numRooms = 0;
        while (numRooms < 8) {
            numRooms = ((new java.util.Random()).nextInt() + 8) % (Math.abs(FORMULAPUSH % COUNTERBALANCE)) + 8;
        }
        System.out.println("Number of rooms to generate: " + numRooms);
        while (current == null) {
            Room.resetTreasure();
            current = GENERATOR.generate(numRooms, FORMULAPUSH, COUNTERBALANCE);
        }
        //GAMEDUNGEON.init(GENERATOR,numRooms, FORMULAPUSH, COUNTERBALANCE);
        computer = GENERATOR.computer;
        current = GENERATOR.getRoot();
    }

    @Override
    public void playGame() {
        for (int i = 0; i < 3000; i++) {
            System.out.println();
        }
        System.out.println("Welcome to the dungeon, the orb is the key, Find it before the computer and you are free, if you don't you'll be lost to eternity.");
        System.out.println("I caution you though, any action you make, whether you run into a wall or go through a door, will take a turn and let the computer get closer, or further from the orb.");
        System.out.println("Use the commands given and the map to move your player.");
        System.out.println("Enjoy!");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException IE) {
            System.err.println("Again, this is not multithreaded so I don't see how this can happen");
        }
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        while (getRoot() != null) {
            boolean goOn = false;
            while (!goOn) {
                current.displayRoom();
                System.out.println("Commands: N,E,S,W,ORB");
                String command = scanner.next();
                if (command.equalsIgnoreCase("N")) {
                    move(NORTH);
                    goOn = true;
                } else if (command.equalsIgnoreCase("S")) {
                    move(SOUTH);
                    goOn = true;
                } else if (command.equalsIgnoreCase("E")) {
                    move(EAST);
                    goOn = true;
                } else if (command.equalsIgnoreCase("W")) {
                    move(WEST);
                    goOn = true;
                } else if (command.equalsIgnoreCase("ORB"))
                    if (checkOrb()) {
                        System.out.println("You Win");
                        System.exit(0);
                    } else System.out.println("Sorry, no orb.");
                int dec = Math.abs(rand.nextInt()) % 4 + 1;
                moveComputer(dec);
                if (computerCheckOrb()) {
                    System.out.println("The Computer Wins");
                    System.exit(0);
                    //GENERATOR.reset();
                }

            }
        }
        //Part of an eventual way to restart the game that is difficult to implement right now.
        /*System.out.println("You can try again if you'd like to: just type 'a' or any other key to exit");
        String command = scanner.next();
        if(command.equalsIgnoreCase("a")){
            setup();
            playGame();
        }*/
    }

    @Override
    public Room getRoot() {
        return GENERATOR.getRoot();
    }

    @Override
    public boolean checkOrb() {
        System.out.println("Room " + current.getRoomNumber() + " has orb: " + current.checkTreasure());
        return current.checkTreasure();
    }

    public boolean computerCheckOrb() {
        System.out.println("Room " + computer.getRoomNumber() + " has orb: " + computer.checkTreasure());
        return computer.checkTreasure();
    }

    @Override
    public void move(int Direction) {
        if (Direction == NORTH && current.north != null) current = current.north;
        else if (Direction == SOUTH && current.south != null) current = current.south;
        else if (Direction == EAST && current.east != null) current = current.east;
        else if (Direction == WEST && current.west != null) current = current.west;
    }

    public void moveComputer(int Direction) {
        computer.removeComputer();
        if (Direction == NORTH && computer.north != null) computer = computer.north;
        else if (Direction == SOUTH && computer.south != null) computer = computer.south;
        else if (Direction == EAST && computer.east != null) computer = computer.east;
        else if (Direction == WEST && computer.west != null) computer = computer.west;
        computer.addComputer();
    }

    /**
     * Extra Items By Class
     * Room:
     *      add(Player)
     *      remove(Player)
     * Player:
     *      getRoom()
     * @param comp
     * @param Direction
     */
    public void move(Player comp, int Direction){
        /*
        Room room = comp.getRoom();
        room.remove(comp);
        if (Direction == NORTH && room.north != null) room = room.north;
        else if (Direction == SOUTH && room.south != null) room = room.south;
        else if (Direction == EAST && room.east != null) room = room.east;
        else if (Direction == WEST && room.west != null) room = room.west;
        room.add(comp);
         */
    }
}
