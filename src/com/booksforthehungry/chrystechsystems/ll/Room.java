package com.booksforthehungry.chrystechsystems.ll;

import java.util.Random;

/**
 * Project dungeonGame created on 3/16/2017, Package com.booksforthehungry.chrystechsystems.ll.
 */
public class Room {
    private boolean hasTreasure = false;
    private static boolean aRoomHasTreasure = false;
    private boolean hasComputer = false;
    private int roomNumber = -1;
    int slotsFull = 0;
    Room north = null;
    Room south = null;
    Room east = null;
    Room west = null;

    public Room(int roomNumber) {
        this.roomNumber = roomNumber;
    }


    public static void resetTreasure(){
        Room.aRoomHasTreasure = false;
    }

    public void addComputer(){
        hasComputer = true;
    }
    public void removeComputer(){
        hasComputer = false;
    }
    boolean attachRoom(Room rm) {
        Random x = new Random();
        int decide = x.nextInt() % 4;
        switch (decide) {
            case 0:
                return tryAddCtrl(rm, "north", "east", "south", "west");
            case 1:
                return tryAddCtrl(rm, "east", "south", "west", "north");
            case 2:
                return tryAddCtrl(rm, "south", "west", "north", "east");
            case 3:
                return tryAddCtrl(rm, "west", "north", "east", "south");
            default:
                return false;
        }
    }

    private boolean tryAddCtrl(Room rm, String dir1, String dir2, String dir3, String dir4) {
        if (tryAdd(rm, dir1)) return true;
        if (tryAdd(rm, dir2)) return true;
        if (tryAdd(rm, dir3)) return true;
        if (tryAdd(rm, dir4)) return true;
        return false;
    }

    private boolean tryAdd(Room rm, String direction) {
        if (direction.equalsIgnoreCase("North") && north == null) {
            north = rm;
            slotsFull += 1;
            return true;
        }
        if (direction.equalsIgnoreCase("South") && south == null) {
            south = rm;
            slotsFull += 1;
            return true;
        }
        if (direction.equalsIgnoreCase("East") && east == null) {
            east = rm;
            slotsFull += 1;
            return true;
        }
        if (direction.equalsIgnoreCase("West") && west == null) {
            west = rm;
            slotsFull += 1;
            return true;
        }
        return false;
    }

    public void addTreasure() {
        if (!aRoomHasTreasure){
            hasTreasure = true;
            aRoomHasTreasure = true;
            System.out.println("DEBUG: PLEASE REMOVE\n ROOM " + roomNumber + " now has Treasure");
        }
    }

    public boolean checkTreasure() {
        return hasTreasure;
    }

    public int getRoomNumber(){
        return roomNumber;
    }
  //  public void displayAllRooms(){

    //}
    public void displayRoom() {
        //TODO add body
        String[][] room = buildRoom();
        for(int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++){
                if(room[i][j] != null) {
                    System.out.print(room[i][j]);
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }

    private String[][] buildRoom() {
        String[][] room = new String[9][9];
        room[0][0] = "+";
        room[8][8] = room[0][0];
        room[0][8] = room[0][0];
        room[8][0] = room[0][0];
        for (int i = 1; i<8; i++){
            room[0][i] = "--";
            room[8][i] = "--";
            room[i][0] = "|";
            room[i][8] = "|";
        }
        if (north != null) {
            room[0][3] = "==";
            room[0][4] = "==";
            room[0][5] = "==";
        }if (south != null) {
            room[8][3] = "==";
            room[8][4] = "==";
            room[8][5] = "==";
        }if (east != null) {
            room[3][8] = "#";
            room[4][8] = "#";
            room[5][8] = "#";
        }if (west != null) {
            room[3][0] = "#";
            room[4][0] = "#";
            room[5][0] = "#";
        }
        if(hasTreasure){
            room[3][3] = "O ";
        }
        if(hasComputer){
            room[5][5] = "C ";
        }
        room[4][4] = "U ";
        room[1][1] = "R ";
        room[1][2] = ": ";
        room[1][3] = "  ";
        room[1][4] = "" + getRoomNumber() + " ";
        return room;
    }

}
