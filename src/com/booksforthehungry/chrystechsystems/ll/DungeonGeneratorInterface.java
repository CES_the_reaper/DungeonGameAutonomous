package com.booksforthehungry.chrystechsystems.ll;

/**
 * Project dungeonGame created on 3/21/2017, Package com.booksforthehungry.chrystechsystems.ll.
 */
public interface DungeonGeneratorInterface {
    int outlets = 4;
    public int PLAYER = 1;
    public int COMPUTER = 2;
    public int TREASURE = 3;
    public Room getRoot();
    public Room generate(int numberOfRoomsToGenerate, int formulaPush, int counterBalance);
    public int randomPlacement(int itemType, int formulaPush, int counterBalance);
}
